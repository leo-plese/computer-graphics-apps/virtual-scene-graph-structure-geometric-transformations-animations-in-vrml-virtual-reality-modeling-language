#VRML V2.0 utf8

WorldInfo {
    title "Avion"
}

Viewpoint {
	# postavljanje pozicije pogleda na scenu
	position 0 0.5 6
    description "Entry view"
}

NavigationInfo {
    type [ "EXAMINE", "ANY" ]
    headlight TRUE
}

Background {
	# plava boja pozadine simulira nebo kojim leti avion
    skyColor    [ 0.0 0.0 1.0 ]
    groundColor [ 0.0 0.0 1.0 ]
}


# TRUP (transformacijski cvor s transformacijskim cvorovima za lijevi/desni dio, rep i inicijale redom)
DEF Trup Transform {
	# translacija i rotacija trupa aviona sa svim nadovezujucim dijelovima (tj. cijelog aviona)
	translation 0 0 -2
	rotation -0.3 1 0.3 -0.6
	
    children [
		# transformacijski cvor za lijevi/desni dio od trupa i trup aviona
		Transform {
			# osnovno skaliranje trupa na u redu velicinu
			scale 1.9 0.3 0.4
			children [
				# oblik kugle - model trupa
				DEF Kugla1 Shape {
					appearance Appearance {
						material Material {
							diffuseColor 1.0 1.0 1.0
						}
						# tekstura trupa - preslikana slika "trup.jpg"
						texture ImageTexture {
							url "trup.jpg"
						}
					  }
					  # geometrija kugle koja se naknadno transformira cvorom osnovnog skaliranja trupa
					  geometry Sphere {
					  }
				}
				  
				# DESNI DIO (desno krilo s motorom s propelerom)
				Transform {
					# osnovna translacija, rotacija i skaliranje desnog dijela obzirom na trup
					translation 0 -0.2 3.4
					rotation 0.0 1.0 0.0 -0.04					
					scale 0.25 0.2 4.5
					children [
						# model krila
						DEF Wing Shape {
							appearance Appearance {
								material Material {
									diffuseColor 1.0 1.0 1.0
								}	
							}
							
							geometry Sphere {
							}
						}
						
						# transformacijski cvor za motor s propelerima
						Transform {
							# osnovna translacija, rotacija i skaliranje motora s propelerima obzirom na krilo
							translation 0.4 0 0.0
							rotation 0.0 1.0 0.0 0.04
							scale 1.0 3 0.1
							children [
								# model motora
								DEF Motor Shape {
									appearance Appearance {
										material Material {
											diffuseColor 1.0 1.0 1.0
										}
									}
									geometry Sphere {
									}
								}
								
								# translacija i skaliranje jedne polovice propelera obzirom na motor
								Transform {
									translation 0.9 0.0 0.0
									scale 0.1 1.2 0.1
									children [
										# model polovice propelera
										DEF Propeller Shape {
											appearance Appearance {
												material Material {
													diffuseColor 0.5 0.5 0.5
												}
											}
											geometry Sphere {
											}
										}
									]
								}
								
								# translacija i skaliranje druge polovice propelera obzirom na motor
								Transform {
									translation 0.9 0.0 0.0
									scale 0.1 1.2 0.1
									rotation 1 0 0 1.57
									children [
										# koristenje modela polovice propelera
										USE Propeller
									]
								}
							]
						}
					]
				}
				  
				# LIJEVI DIO (lijevo krilo s motorom s propelerom)
				Transform {
					# osnovna translacija, rotacija i skaliranje lijevog dijela obzirom na trup
					translation 0 -0.2 -3.4
					rotation 0.0 1.0 0.0 0.04
					scale 0.25 0.2 4.5
					children [
						# koristenje modela krila
						USE Wing

						# transformacijski cvor za motor s propelerima
						Transform {
							# osnovna translacija, rotacija i skaliranje motora s propelerima obzirom na krilo
							translation 0.4 0 0.0
							rotation 0.0 1.0 0.0 -0.04
							scale 1.0 3 0.1
							children [
								# koristenje modela motora
								USE Motor
								
								# translacija i skaliranje jedne polovice propelera obzirom na motor
								Transform {
									translation 0.9 0.0 0.0
									scale 0.1 1.2 0.1
									children [
										# koristenje modela polovice propelera
										USE Propeller
									]
								}
								
								# translacija i skaliranje druge polovice propelera obzirom na motor
								Transform {
									translation 0.9 0.0 0.0
									scale 0.1 1.2 0.1
									rotation 1 0 0 1.57
									children [
										# koristenje modela polovice propelera
										USE Propeller
									]
								}
							]
						}
						
						
					]
				}
			]
		}
		
		# REP (transformacijski cvor s cvorom za rep i transformacijskim cvorovima za repni motor s propelerima, lijevo i desno repno krilo redom)
		Transform {
			# translacija, rotacija i skaliranje repa aviona sa svim nadovezujucim dijelovima (motor s propelerima i 2 krila)
			translation -1.5 0.25 0.0
			rotation 0.0 0.0 1.0 -0.8
			scale 0.5 0.2 0.075
			children [
				# model repa
				DEF Kugla2 Shape {
					appearance Appearance {
						material Material {
							diffuseColor 1.0 1.0 1.0
						}
					}
					geometry Sphere {
					}
				}
				
				# transformacijski cvor za motor s propelerima
				Transform {
					# osnovna translacija, rotacija i skaliranje motora s propelerima obzirom na krilo
					translation -0.3 1.2 0.0
					rotation 0.0 0.0 1.0 1.25
					scale 2.0 0.35 1.0
					children [
						# koristenje modela motora
						USE Motor
			
						# translacija i skaliranje jedne polovice propelera obzirom na motor
						Transform {
							translation 0.9 0.0 0.0
							rotation 0 0 1 -0.2
							scale 0.1 1.2 0.1
							children [
								# koristenje modela polovice propelera
								USE Propeller
							]
						}	
						
						# translacija i skaliranje druge polovice propelera obzirom na motor
						Transform {
							translation 0.9 0.0 0.0
							rotation 1 0 0 1.57
							scale 0.1 1.2 0.1
							children [
								# koristenje modela polovice propelera
								USE Propeller
							]
						}
					]
				}
				
				# transformacijski cvor za lijevo repno krilo
				Transform {
					# translacija, rotacija i skaliranje (obzirom na rep)
					translation -0.8 0.4 -6
					rotation 1.0 0.2 0.0 0.05
					scale 0.1 0.5 8
					children [
						# koristenje modela krila
						USE Wing
					]
				}
				
				# transformacijski cvor za desno repno krilo
				Transform {
					# translacija, rotacija i skaliranje (obzirom na rep)
					translation -0.8 0.4 6
					rotation 1.0 0.2 0.0 -0.05
					scale 0.1 0.5 8
					children [
						# koristenje modela krila
						USE Wing
					]
				}
			]
		} 
	  
		# crtanje incijala LP na desnom krilu aviona
		# transformacijski cvor koji ce (translacijom i rotacijom) dovesti oblik inicijala na poziciju desnog krila
		Transform {
			# translacija i rotacija (obzirom na trup)
			translation -0.1 0.1 2.5
			rotation 1.0 0.0 0 -1.57
			children [
				# model inicijala
				Shape {
					appearance Appearance {
						# slova su crne boje
						material Material {
							diffuseColor 0.0 0.0 0.0
						}
					}
					# geometrijski oblik teksta za inicijale
					geometry Text {
						# ispis inicijala - tekst "LP" u stilu zadane velicine fonta
						string [ "LP" ]
						fontStyle FontStyle {
							size 0.5
						}
					}
				}
			]
		}
    ]
}









