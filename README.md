# Virtual Scene Graph Structure Geometric Transformations Animations in VRML Virtual Reality Modeling Language

Virtual scene graphs defining models, simulations (animations) and application of body textures implemented in VRML.

Implemented VRML scene are defined in .wrl files. Results are listed and described in "ResultsReport.pdf".

My lab assignment in Introduction to Virtual Environments, FER, Zagreb.

Task descriptions in "TaskSpecification.pdf".

Created: 2020